import { getClient } from '@client';
import config from '@config';

import {
  AutocompleteInteraction,
  ChatInputCommandInteraction,
  GuildBasedChannel,
  GuildMember,
  Message,
  TextChannel,
} from 'discord.js';
import { DiscordBotCommandName } from '@commands/types';
import CommandMap from './commands';

const GUILD_ID = config.discord.guildId;

export function memberHasRole(member: GuildMember, roleName: string): boolean {
  return member.roles.cache.some((role) => role.name === roleName);
}

export async function getGuildMemberByDiscordId(
  discordId: string,
): Promise<GuildMember> {
  const client = getClient();

  // First try fetching via user guild cache
  const guild = await client.guilds.fetch(GUILD_ID);

  if (!guild) {
    throw new Error(`Failed to look up guild ${GUILD_ID}`);
  }

  const cachedGuildMember = guild.members.cache.get(discordId);
  const foundMember = await (cachedGuildMember
    ? cachedGuildMember
    : guild.members.fetch(discordId));

  if (!foundMember) {
    throw new Error(`Failed to look up user ${discordId}`);
  }

  return foundMember;
}

export async function sendUserDM(discordId: string, message: string) {
  try {
    const guildMember = await getGuildMemberByDiscordId(discordId);
    await guildMember.send(message);
  } catch (error) {
    console.error(`Failed to send DM to GuildMember ${discordId}: ${message}`);
    console.error(error);
  }
}

export async function getAllChannels() {
  const client = getClient();
  const guild = await client.guilds.fetch(GUILD_ID);
  const cachedChannels = guild.channels.cache.values();
  const channelArray: GuildBasedChannel[] = [];
  for (const channel of cachedChannels) {
    channelArray.push(channel);
  }

  return channelArray;
}

export async function getChannel(channelId: string): Promise<TextChannel> {
  const client = await getClient();
  const channel = (await client.channels.fetch(channelId)) as TextChannel;
  if (channel) {
    return channel;
  } else {
    throw new Error(`Failed to find channel ${channelId}`);
  }
}

export async function getMessage(
  messageId: string,
  channelId: string,
): Promise<Message<boolean>> {
  const channel = await getChannel(channelId);
  const message = await channel.messages.fetch(messageId);

  if (message) {
    return message;
  }

  throw new Error(
    `Failed to find message ${messageId} in channel ${channelId}`,
  );
}

export async function sendInChannel(channelId: string, message: string) {
  const client = await getClient();
  const channel = (await client.channels.fetch(channelId)) as TextChannel;
  if (channel) {
    await channel.send(message);
  } else {
    throw new Error(`Failed to find channel ${channelId}`);
  }
}

export function getHandlersForCommand(
  command: DiscordBotCommandName,
  subcommand: string,
) {
  const commandDef = CommandMap[command];

  if ('subcommands' in commandDef) {
    const subcommandDef = commandDef.subcommands[subcommand];

    if (!subcommandDef) {
      throw new Error(
        `${command} uses subcommands but has no subcommand handler for ${subcommand}`,
      );
    }

    return {
      handle: subcommandDef.handle,
      autocomplete: subcommandDef.autocomplete,
    };
  }

  return {
    handle: commandDef.handle,
    autocomplete: commandDef.autocomplete,
  };
}

export function extractImageForMessage(message: Message<boolean>): string {
  if (message.attachments.size > 0) {
    const firstAttachment = message.attachments.first();
    if (firstAttachment?.contentType?.includes('image')) {
      return firstAttachment.url;
    }
  }

  if (message.embeds.length > 0) {
    const firstEmbed = message.embeds[0];
    const thumbnailData = firstEmbed.data.thumbnail;

    if (thumbnailData) {
      return thumbnailData.proxy_url || thumbnailData.url;
    }
  }

  return '';
}

export function getCommandAndSubcommandFromInteraction(
  interaction: ChatInputCommandInteraction | AutocompleteInteraction,
) {
  const command = interaction.commandName;
  let subcommand = '';
  try {
    subcommand = interaction.options.getSubcommand();
  } catch (e) {
    /* Noop */
  }

  return {
    command,
    subcommand,
  };
}
