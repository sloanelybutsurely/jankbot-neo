import * as dotenv from 'dotenv';
dotenv.config();

const configValues = {
  discord: {
    enabled: true,
    clientId: process.env.DISCORD_CLIENT_ID ?? '',
    clientSecret: process.env.DISCORD_CLIENT_SECRET ?? '',
    guildId: process.env.DISCORD_GUILD_ID ?? '',
    token: process.env.DISCORD_TOKEN ?? '',
  },
};

export default configValues;
