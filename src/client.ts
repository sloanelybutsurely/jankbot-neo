import { Client, GatewayIntentBits, Partials } from 'discord.js';
import config from '@config';

const client = new Client({
  intents: [
    // Essentially everything
    GatewayIntentBits.Guilds,
    GatewayIntentBits.DirectMessageReactions,
    GatewayIntentBits.DirectMessageTyping,
    GatewayIntentBits.GuildBans,
    GatewayIntentBits.GuildEmojisAndStickers,
    GatewayIntentBits.GuildIntegrations,
    GatewayIntentBits.GuildInvites,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.GuildMessageTyping,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildPresences,
    GatewayIntentBits.GuildScheduledEvents,
    GatewayIntentBits.GuildVoiceStates,
    GatewayIntentBits.GuildWebhooks,
    GatewayIntentBits.MessageContent,
  ],

  // Enabling partials allows us to get reaction events for uncached messages
  partials: [Partials.Reaction, Partials.Message],
});

export async function login() {
  await client.login(config.discord.token);
  await client.guilds.fetch();
  return;
}

export function getClient() {
  return client;
}
