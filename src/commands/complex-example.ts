import {
  AutocompleteInteraction,
  ChatInputCommandInteraction,
  SlashCommandBuilder,
} from 'discord.js';
import { DiscordCommandHandler } from '@commands/types';

const COMMAND = 'complex-example';
const DESCRIPTION =
  'A complex example command with subcommands and dynamic autocompletion';

const ComplexExampleCommandHandler: DiscordCommandHandler = {
  command: COMMAND,

  description: DESCRIPTION,

  build() {
    return (
      new SlashCommandBuilder()
        .addSubcommand((subcommand) =>
          subcommand
            .setName('example-sub')
            .setDescription(
              'An example subcommand. It has one parameter with autocompletion',
            )
            .addStringOption((option) =>
              option
                .setName('someparam')
                .setDescription('An example parameter')
                .setRequired(true)
                .setAutocomplete(true),
            ),
        )
        .addSubcommand((subcommand) =>
          subcommand
            .setName('greeting')
            .setDescription(
              'Another example subcommand. It has two parameters with no autocompletion',
            )
            .addStringOption((option) =>
              option
                .setName('name')
                .setDescription('Your name')
                .setRequired(true),
            )
            .addIntegerOption((option) =>
              option
                .setName('age')
                .setDescription('Your age')
                .setMinValue(0)
                .setMaxValue(120)
                .setRequired(true),
            ),
        )
        // Note: Always put `setName` and `setDescription` last for ~reasons~
        .setName(COMMAND)
        .setDescription(DESCRIPTION)
    );
  },

  subcommands: {
    'example-sub': {
      async autocomplete(
        interaction: AutocompleteInteraction,
        focusedValue: string,
      ) {
        // This is static here but you can do whatever on the fly
        return [
          {
            name: 'Option 1',
            value: 'option-1',
          },
          {
            name: 'Option 2',
            value: 'option-2',
          },
        ].filter((option) =>
          option.name.toLowerCase().includes(focusedValue.toLowerCase()),
        );
      },

      async handle(interaction: ChatInputCommandInteraction) {
        const parameterValue = interaction.options.getString('someparam') ?? '';
        await interaction.reply({
          content: `You chose ${parameterValue}!`,
        });
      },
    },

    greeting: {
      async handle(interaction: ChatInputCommandInteraction) {
        const name = interaction.options.getString('name') ?? 'Someone';
        const age = interaction.options.getInteger('age') ?? 10;

        await interaction.reply({
          content: `Hello ${name}! You are ${age} years old.`,

          // Setting `ephemeral` to `true` makes the reply only appear to the user who used the command
          ephemeral: true,
        });
      },
    },
  },
};

export default ComplexExampleCommandHandler;
