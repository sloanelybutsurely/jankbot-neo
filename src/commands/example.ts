import { ChatInputCommandInteraction, SlashCommandBuilder } from 'discord.js';
import { DiscordCommandHandler } from '@commands/types';

const COMMAND = 'example';
const DESCRIPTION = 'A simple example command';

const ExampleCommandHandler: DiscordCommandHandler = {
  command: COMMAND,

  description: DESCRIPTION,

  build() {
    return (
      new SlashCommandBuilder()
        // Note: Always put `setName` and `setDescription` last for ~reasons~
        .setName(COMMAND)
        .setDescription(DESCRIPTION)
    );
  },

  async handle(interaction: ChatInputCommandInteraction) {
    await interaction.reply({
      content: 'Hello!',
    });
  },
};

export default ExampleCommandHandler;
