import type {
  ChatInputCommandInteraction,
  AutocompleteInteraction,
  SlashCommandBuilder,
  ApplicationCommandOptionChoiceData,
} from 'discord.js';

export const COMMANDS = ['example', 'complex-example'] as const;

export type DiscordBotCommandName = typeof COMMANDS[number];

export function isDiscordBotCommand(
  input: string,
): input is DiscordBotCommandName {
  return ([...COMMANDS] as string[]).includes(input);
}

export type CommandHandlerFunction = (
  interaction: ChatInputCommandInteraction,
) => Promise<void>;

export type DiscordCommandAutocompleter = (
  interaction: AutocompleteInteraction,
  focusedValue: string,
) => Promise<ApplicationCommandOptionChoiceData<string | number>[]>;

export interface DiscordSubcommandHandler {
  handle: CommandHandlerFunction;
  autocomplete?: DiscordCommandAutocompleter;
}

export type DiscordCommandHandler =
  | {
      command: DiscordBotCommandName;
      description: string;
      handle: CommandHandlerFunction;
      build: () => SlashCommandBuilder;
      autocomplete?: DiscordCommandAutocompleter;
    }
  | {
      command: DiscordBotCommandName;
      description: string;
      build: () => SlashCommandBuilder;
      subcommands: Record<string, DiscordSubcommandHandler>;
    };
