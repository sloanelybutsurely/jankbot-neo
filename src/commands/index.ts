import ExampleCommandHandler from './example';
import ComplexExampleCommandHandler from './complex-example';

import type {
  DiscordBotCommandName,
  DiscordCommandHandler,
} from '@commands/types';

const CommandMap: Record<DiscordBotCommandName, DiscordCommandHandler> = {
  'complex-example': ComplexExampleCommandHandler,
  example: ExampleCommandHandler,
};

export default CommandMap;
