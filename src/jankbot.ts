import { getClient, login } from '@client';
import { isDiscordBotCommand } from '@commands/types';
import { Events, Message, MessageReaction } from 'discord.js';
import {
  getCommandAndSubcommandFromInteraction,
  getHandlersForCommand,
} from './util';

export async function launchDiscordBot() {
  console.log('Initializing bot...');
  const client = getClient();

  // Handle ready
  client.once('ready', () => {
    console.log('Discord bot ready.');
  });

  client.on(Events.MessageReactionAdd, async (reaction, user) => {
    let fullReaction: MessageReaction;
    let fullMessage: Message<boolean>;
    try {
      fullReaction = await reaction.fetch();
    } catch (error) {
      console.error(
        `Something went wrong when fetching the message reaction: ${error}`,
      );
      return;
    }

    if (reaction.message.partial) {
      try {
        fullMessage = await reaction.message.fetch();
      } catch (error) {
        console.error(
          `Something went went wrong when fetching the message: ${error}`,
        );
      }
    } else {
      fullMessage = reaction.message as Message<boolean>;
    }

    // PLACEHOLDER: Handle the incoming reaction
  });

  client.on(Events.ThreadCreate, async (newThread, wasCreated) => {
    // PLACEHOLDER: Handle thread creation
  });

  // Handle new users
  client.on(Events.GuildMemberAdd, async (guildMember) => {
    // PLACEHOLDER: Handle a new member
  });

  // Handle incoming messages
  client.on(Events.MessageCreate, async (message) => {
    // Short circuit for broken messages, messages without an author, or bots
    if (!message || message.partial || !message.author || message.author.bot) {
      return;
    }

    // PLACEHOLDER: Handle incoming messages
  });

  // Handle commands and autocompletes
  client.on('interactionCreate', async (interaction) => {
    if (!interaction.isChatInputCommand() && !interaction.isAutocomplete()) {
      return;
    }

    const { command, subcommand } =
      getCommandAndSubcommandFromInteraction(interaction);

    // Reject invalid coammnds
    if (!isDiscordBotCommand(command)) {
      throw new Error(
        'User is attempting to invoke a command that does not exist: ' +
          command,
      );
    }

    // Main command execution
    if (interaction.isChatInputCommand()) {
      const { handle } = getHandlersForCommand(command, subcommand);
      await handle(interaction);
    } else {
      // Autocomplete
      const { autocomplete } = getHandlersForCommand(command, subcommand);

      if (autocomplete) {
        const focusedValue = interaction.options.getFocused();
        const autocompleteOptions = await autocomplete(
          interaction,
          focusedValue,
        );
        return interaction.respond(autocompleteOptions);
      }
    }
  });

  return login();
}

launchDiscordBot();
