import { Routes } from 'discord.js';
import { REST } from '@discordjs/rest';
import config from '@config';
import { map } from 'lodash';
import CommandMap from '@commands/index';

import type { DiscordCommandHandler } from '@commands/types';

const clientId = config.discord.clientId;
const guildId = config.discord.guildId;
const token = config.discord.token;

const commands = map(CommandMap, (handler: DiscordCommandHandler) =>
  handler.build(),
);

const rest = new REST({ version: '10' }).setToken(token);

type ResponseWithLength = {
  length: number;
};

function isResponseWithLength(object: unknown): object is ResponseWithLength {
  return object !== null && typeof object === 'object' && 'length' in object;
}

async function main() {
  // First, clear commands
  const resp1 = await rest.put(
    Routes.applicationGuildCommands(clientId, guildId),
    { body: [] },
  );
  if (isResponseWithLength(resp1)) {
    console.log(`Successfully cleared commands.`);
  }

  // Then, register commands
  const resp2 = await rest.put(
    Routes.applicationGuildCommands(clientId, guildId),
    { body: commands },
  );
  if (isResponseWithLength(resp2)) {
    console.log(
      `Successfully registered ${resp2.length} application commands.`,
    );
    process.exit(0);
  } else {
    console.log(
      `Failed to register commands. Repsonse: ${JSON.stringify(resp2)}`,
    );
  }
}

main();
