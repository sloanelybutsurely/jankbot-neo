# Jankbot Neo

_Surely this is a good idea_

## Installing

```sh
npm install
```

## Config

Make a .env file from the template:

```sh
cp env.template .env
```

Fill out the env variables using the values from your Discord bot. You should [make your own bot](https://discord.com/developers/) and invite it to a test server to develop.

Discord.js has a guide to creating a bot [here](https://discordjs.guide/preparations/setting-up-a-bot-application.html).

## Run

First, you need to "deploy" the commands. This registers your commands with Discord. You'll need to run this script every time you change or add a command, subcommand or parameter:

```sh
# Build and deploy in one command
npm run dev-deploy-commands

# Or, if you've built the codebase already
npm run deploy-commands
```

Then, to boot up the bot in development mode...

```sh
npm run dev
```

Or to boot the built code:

```sh
npm start
```
